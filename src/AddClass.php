<?php
namespace App;

class AddClass
{
    /**
     * Add two numbers
     * @param  int $num1
     * @param  int $num2
     * @return int
     */
    public function add($num1, $num2)
    {
        return (int)$num1 + (int)$num2;
    }
}
