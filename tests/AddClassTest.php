<?php
namespace App\Tests;

use App\AddClass;

class AddClassTest extends \PHPUnit_Framework_TestCase
{
    protected $adder;

    protected function setUp()
    {
        $this->adder = new AddClass;
    }

    public function testAdd()
    {
        $arg1 = 2;
        $arg2 = 5;
        $this->assertEquals(7, $this->adder->add($arg1, $arg2));
    }

    public function testAddNotEqual()
    {
        $arg1 = 1;
        $arg2 = 2;
        $this->assertNotEquals(4, $this->adder->add($arg1, $arg2));
    }

    public function testAddFailureCase()
    {
        $arg1 = 2;
        $arg2 = 5;
        $this->assertEquals(8, $this->adder->add($arg1, $arg2));
    }
}
